# Development

## Environment

### [PDM](https://pdm.fming.dev/latest/) 
The following commands use PDM to automate the management of the environment.

```bash
# Install project requirements
pdm sync

# Run the database migrations
pdm run python -m shep_api_server.database.migrations up --rev head --yes

# Run server
pdm run server
```

#### Testing

```bash
# Run tests
pdm run test 
```

### Virtual Environment
This project generates production and development requirements via PDM to allow developers to use virtual environments to interact with the code if they are unable to install and use PDM. 
However, this method is not recommneded as it is not guarenteed to work cross platform and dependency changes should be managed through PDM for stronger consistencies.

#### Creation

```bash
# Initialize empty python virtual environment
python3 -m venv .venv 
```

##### macOS
The latest macOS defaults to python 3.9 for it's system python. By using homebrew we can install python 3.10 and use it for the virtual environment with the following commands:

```bash
brew install python@3.10
$(brew --prefix)/opt/python@3.10/libexec/bin/python3 -m venv .venv
```

#### Install Dependencies
The following commands activate a python virtual environment and install the package and its dependencies.

```bash
# Activate virtual environment
source .venv/bin/activate

# Install project requirements
pip install -r requirements.txt
pip install -r requirements-dev.txt

# Install project as editable requirement
pip install -e . 
```

#### Run local development server
The following will activate the virtual environment and start a local development server which will watch for changes and reload itself.

```bash
# Activate virtual environment
source .venv/bin/activate

# Run server
python -m uvicorn server.main:app --reload
```

#### Testing

```bash
# Activate virtual environment
source .venv/bin/activate

# Run tests
pytest 
```


# Deployment

## Docker
A Dockerfile is provided to compile and run the server. 

The following command builds the docker container and tags it.
```bash
docker build -t shep-api-server:latest .
```

The following command starts the docker container and exposes the webserver on port 8000
```bash
docker run -p 8000:8000 shep-api-server:latest
```

## Continious Integration
Local CI execution via [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local) can be used whilst developing or testing using the following command. The `no-artifacts-to-source` flag stops the artifacts from each step from being copied to the local source directory.

```bash
gitlab-ci-local --no-artifacts-to-source
```