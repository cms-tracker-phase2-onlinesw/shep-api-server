{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    gitignore,
    flake-parts,
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        ./nix/overlay.nix
        ./nix/package/default.nix
      ];
      systems = ["x86_64-linux"];
      perSystem = {inputs', ...}: {
        formatter = inputs'.nixpkgs.legacyPackages.alejandra;
      };
    };
}
