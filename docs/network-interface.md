# Shep API Reference Guide

## Contents
[[_TOC_]] 

## Board

### Registration
To register a board, the following endpoint is used:
```
POST api/v1/boards/
```
with the following data parameters:
| Parameter | Type   | Example values                    |
| ---       | ---    | ---                               |
| `ip`      | string | `"127.0.0.1"`                     |
| `type`    | string | `"herd-dummy"`, `"herd-serenity"` |

For example, here is a cURL command to register a board:
```shell
curl -X POST http://localhost/api/v1/boards/ -d ip="127.0.0.1" -d type="herd-dummy"
```

### Delete
To delete a board, the following endpoint is used:
```
DELETE api/v1/boards/<uuid>/
```
For example, here is a cURL command to delete a board:
```shell
curl -X DELETE http://localhost/api/v1/boards/96f8f312-8481-4151-b836-b01b41c5cba9/
```

### Update
To update a board, the following endpoint is used:
```
PATCH api/v1/boards/<uuid>/
```
with the following data parameters:
| Parameter | Type    | Example values                    |
| ---       | ---     | ---                               |
| `ip`      | string  | `"127.0.0.1"`                     |
| `type`    | string  | `"herd-dummy"`, `"herd-serenity"` |
| `online`  | boolean | `true`, `false`                   |

NOTE: you only need to provide the data fields you wish to update. Any data fields which you would like to remain the same can be omitted.

For example, here is a cURL command to update a board:
```shell
curl -X PATCH http://localhost/api/v1/boards/96f8f312-8481-4151-b836-b01b41c5cba9/ -d type="herd-serenity"
```

### HERD API
All herd-control-app API endpoints are available as a suffix to `api/v1/boards/<uuid>/` for the HERD instance that is associated with the UUID. All data parameters are similarly passed onto the HERD instance and the results are forwarded back to the user. An example cURL command is as follows:
```
curl -X GET http://localhost/api/v1/boards/96f8f312-8481-4151-b836-b01b41c5cba9/devices
```
And would return the device information for the HERD instance.

## Application

### Registration
To register an application, the following endpoint is used:
```
POST api/v1/applications/
```
with the following data parameters:
| Parameter | Type                      | Example values                    |
| ---       | ---                       | ---                               |
| `name`    | string                    | `"test-application"`              |
| `boards`  | postgreSQL list of UUIDs  | `"{96f8f312-8481-4151-b836-b01b41c5cba9, ee448c65-b15b-4141-a827-20c3fc1cb9c4}"` |

For example, here is a cURL command to register an application:
```shell
curl -X POST http://localhost/api/v1/applications/ -d name="test-application" -d boards="{96f8f312-8481-4151-b836-b01b41c5cba9, ee448c65-b15b-4141-a827-20c3fc1cb9c4}"
```

### Delete
To delete an application, the following endpoint is used:
```
DELETE api/v1/applications/<uuid>/
```
For example, here is a cURL command to delete an application:
```shell
curl -X DELETE http://localhost/api/v1/applications/bddd3b97-a2af-4a40-847e-3e82ed16465e/
```

### Update
To update an application, the following endpoint is used:
```
PATCH api/v1/applications/<uuid>/
```
with the following data parameters:
| Parameter | Type                      | Example values                    |
| ---       | ---                       | ---                               |
| `name`    | string                    | `"test-application"`              |
| `boards`  | postgreSQL list of UUIDs  | `"{96f8f312-8481-4151-b836-b01b41c5cba9, ee448c65-b15b-4141-a827-20c3fc1cb9c4}"` |

NOTE: you only need to provide the data fields you wish to update. Any data fields which you would like to remain the same can be omitted.

For example, here is a cURL command to update an application:
```shell
curl -X PATCH http://localhost/api/v1/applications/bddd3b97-a2af-4a40-847e-3e82ed16465e/ -d name="updated-application"
```

### HERD API
All herd-control-app API endpoints are available as a suffix to `api/v1/applications/<uuid>/` for all the HERD instances that is associated with the application. All data parameters are similarly passed onto the HERD instances and the results are collated and forwarded back to the user. An example cURL command is as follows:
```
curl -X GET http://localhost/api/v1/applications/bddd3b97-a2af-4a40-847e-3e82ed16465e/devices
```
And would return the device information for the HERD instances.
