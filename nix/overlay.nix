{
  inputs,
  self,
  ...
}: {
  flake = {
    overlays.default = final: prev: {
      gitlab-ci-local = final.callPackage ./package/gitlab-ci-local/default.nix {};
      shep-api-server = with prev.python3Packages; toPythonApplication shep-api-server;

      pythonPackagesExtensions =
        prev.pythonPackagesExtensions
        ++ [
          (pself: pprev: {
            shep-api-server = pself.callPackage ./package/shep-api-server/default.nix {inherit (inputs.gitignore.lib) gitignoreSource;};
          })
        ];
    };
  };
  perSystem = {
    config,
    pkgs,
    system,
    ...
  }: let
    pkgs = import inputs.nixpkgs {
      inherit system;

      overlays = [
        self.overlays.default
      ];
    };
  in {config = {_module.args.pkgs = pkgs;};};
}
