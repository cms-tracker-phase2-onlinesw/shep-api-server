{
  lib,
  pythonOlder,
  buildPythonPackage,
  gitignoreSource,
  pdm-pep517,
  setuptools,
  fastapi,
  aiohttp,
  sqlalchemy,
  ujson,
  orjson,
  uvicorn,
  tenacity,
  alembic,
  platformdirs,
  pytestCheckHook,
  pytest,
  pytest-cov,
  pytest-sugar,
  httpx,
  aiosqlite,
  freezegun,
}:
buildPythonPackage rec {
  pname = "shep-api-server";
  version = "0.3.1";
  format = "pyproject";
  disabled = pythonOlder "3.8";

  HOME = ".";

  src = gitignoreSource ../../../.;

  propagatedBuildInputs =
    [
      fastapi
      aiohttp
      sqlalchemy
      ujson
      orjson
      uvicorn
      tenacity
      pdm-pep517
      alembic
      platformdirs
    ]
    ++ uvicorn.optional-dependencies.standard;

  checkInputs = [pytestCheckHook] ++ passthru.optional-dependencies.test;

  pythonImportsCheck = ["shep_api_server"];

  passthru = {
    optional-dependencies = {
      test = [pytest pytest-cov pytest-sugar httpx aiosqlite freezegun];
    };
    tests = {
    };
  };

  meta = with lib; {
    homepage = "https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server";
    maintainers = with maintainers; [];
  };
}
