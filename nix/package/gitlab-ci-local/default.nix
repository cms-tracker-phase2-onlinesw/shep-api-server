{
  lib,
  buildNpmPackage,
  fetchFromGitHub,
}:
buildNpmPackage rec {
  pname = "gitlab-ci-local";
  version = "4.41.2";

  src = fetchFromGitHub {
    owner = "firecow";
    repo = "gitlab-ci-local";
    rev = version;
    hash = "sha256-vwj1Gty+A5K71zlgz7LigZdOtfeucRIYMONjtgZoyi8=";
  };

  npmDepsHash = "sha256-+b2bb1bZs5BRL3WY5hMmLO/gyMZRdsAguVbPM8ypINA=";

  meta = with lib; {
    description = "Tired of pushing to test your .gitlab-ci.yml";
    homepage = "https://github.com/firecow/gitlab-ci-local";
    license = licenses.mit;
    maintainers = with maintainers; [];
  };
}
