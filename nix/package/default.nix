{lib, ...}: {
  imports = [
    ./shep-api-server/devShell.nix
  ];

  perSystem = {
    config,
    pkgs,
    ...
  }: let
    pythonPackages = lib.filterAttrs (n: v: builtins.match "python3[[:digit:]]+?" n != null) pkgs;
    pythonVersions = lib.filterAttrs (n: v: !v.pythonOlder "3.8") pythonPackages;
  in {
    packages =
      {
        default = pkgs.shep-api-server;
        shep-api-server = pkgs.shep-api-server;
        gitlab-ci-local = pkgs.gitlab-ci-local;
      }
      // (builtins.mapAttrs (name: value: value.withPackages (p: [p.shep-api-server])) pythonVersions);

    devShells.default = config.devShells.shep-api-server;
  };
}
