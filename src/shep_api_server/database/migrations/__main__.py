import logging

import click
from alembic import command
from alembic.config import Config

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)

cfg = Config()
cfg.set_main_option("script_location", "shep_api_server.database:migrations")


@click.group()
@click.option("-v", "--verbose", type=bool, is_flag=True)
def main(verbose: bool):
    logger.setLevel(level="DEBUG" if verbose else "INFO")


@main.command()
@click.option("--rev", type=str, default="head", help="revision to upgrade to")
@click.confirmation_option(prompt="Are you sure you want to upgrade database?")
def up(rev):
    command.upgrade(cfg, rev)


@main.command()
@click.option("--rev", type=str, default="head", help="revision to downgrade to")
@click.confirmation_option(prompt="Are you sure you want to downgrade database?")
def down(rev):
    command.downgrade(cfg, rev)


@main.command()
def history():
    command.history(cfg)


if __name__ == "__main__":
    main()
