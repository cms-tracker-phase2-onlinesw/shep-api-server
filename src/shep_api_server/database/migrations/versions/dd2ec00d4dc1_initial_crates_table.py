"""Initial crates table

Revision ID: dd2ec00d4dc1
Revises: e934374e3d8a
Create Date: 2023-03-09 21:55:11.484698

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dd2ec00d4dc1"
down_revision = "e934374e3d8a"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "crates",
        sa.Column("id", sa.String(), nullable=False),
        sa.Column("building", sa.String(), nullable=False),
        sa.Column("rack", sa.String(), nullable=False),
        sa.Column("height", sa.Integer(), nullable=False),
        sa.Column("description", sa.String(), nullable=False),
        sa.Column("csp_id", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("id"),
    )


def downgrade() -> None:
    op.drop_table("crates")
