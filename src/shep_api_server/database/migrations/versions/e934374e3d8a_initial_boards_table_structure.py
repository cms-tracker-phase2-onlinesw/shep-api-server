"""Initial boards table structure

Revision ID: e934374e3d8a
Revises: 
Create Date: 2023-03-09 13:35:22.955711

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e934374e3d8a"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "board",
        sa.Column("id", sa.String(), nullable=False),
        sa.Column("ip", sa.String(), nullable=False),
        sa.Column("port", sa.Integer(), nullable=False),
        sa.Column(
            "date_registered",
            sa.DateTime(),
            server_default=sa.text("(CURRENT_TIMESTAMP)"),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("id"),
    )


def downgrade() -> None:
    op.drop_table("board")
