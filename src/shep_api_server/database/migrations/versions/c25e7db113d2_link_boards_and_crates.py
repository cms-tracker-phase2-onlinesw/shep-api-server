"""link boards and crates

Revision ID: c25e7db113d2
Revises: dd2ec00d4dc1
Create Date: 2023-03-10 07:56:44.801610

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c25e7db113d2"
down_revision = "dd2ec00d4dc1"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("board", schema=None, recreate="always") as batch_op:
        # type: ignore
        batch_op.add_column(sa.Column("crate_id", sa.String(), nullable=True))
        batch_op.add_column(
            sa.Column("slot", sa.Integer(), nullable=False),
        )
        batch_op.create_foreign_key("fk_crate_id", "crates", ["crate_id"], ["id"])


def downgrade() -> None:
    with op.batch_alter_table("board", schema=None) as batch_op:
        batch_op.drop_constraint("fk_crate_id", type_="foreignkey")
        batch_op.drop_column("crate_id")
        batch_op.drop_column("slot")
