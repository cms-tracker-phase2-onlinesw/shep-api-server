"""create link table

Revision ID: a98b25fe9e6b
Revises: c25e7db113d2
Create Date: 2023-04-06 09:23:41.899973

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a98b25fe9e6b"
down_revision = "c25e7db113d2"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "links",
        sa.Column("id", sa.String(), primary_key=True),
        sa.Column("src_board_id", sa.String()),
        sa.Column("src_processor", sa.String()),
        sa.Column("src_port", sa.Integer()),
        sa.Column("dst_board_id", sa.String()),
        sa.Column("dst_processor", sa.String()),
        sa.Column("dst_port", sa.Integer()),
    )


def downgrade() -> None:
    op.drop_table("links")
