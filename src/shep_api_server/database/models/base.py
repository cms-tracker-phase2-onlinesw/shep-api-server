import datetime

from sqlalchemy import func
from sqlalchemy.orm import DeclarativeBase, mapped_column
from typing_extensions import Annotated

timestamp = Annotated[
    datetime.datetime,
    mapped_column(
        nullable=False,
        default=lambda: datetime.datetime.now(),
        server_default=func.CURRENT_TIMESTAMP(),
    ),
]


class Base(DeclarativeBase):
    pass
