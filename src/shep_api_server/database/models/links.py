from typing import TYPE_CHECKING
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from shep_api_server.database.models.base import Base


if TYPE_CHECKING:
    from shep_api_server.database.models.boards import Board


class Link(Base):
    __tablename__ = "links"

    id: Mapped[str] = mapped_column(primary_key=True)
    src_board_id: Mapped[str] = mapped_column(ForeignKey("board.id"))
    src_processor: Mapped[str]
    src_port: Mapped[int]
    dst_board_id: Mapped[str] = mapped_column(ForeignKey("board.id"))
    dst_processor: Mapped[str]
    dst_port: Mapped[int]

    source: Mapped["Board"] = relationship(foreign_keys=src_board_id)
    destination: Mapped["Board"] = relationship(foreign_keys=dst_board_id)
