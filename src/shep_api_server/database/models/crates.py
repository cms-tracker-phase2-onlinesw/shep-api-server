from typing import TYPE_CHECKING, List

from sqlalchemy.orm import Mapped, mapped_column, relationship

from shep_api_server.database.models.base import Base

# Required to avoid cyclic dependency
if TYPE_CHECKING:
    from shep_api_server.database.models.boards import Board


class Crate(Base):
    __tablename__ = "crates"

    id: Mapped[str] = mapped_column(primary_key=True, unique=True)
    building: Mapped[str]
    rack: Mapped[str]
    height: Mapped[int]
    description: Mapped[str]
    csp_id: Mapped[int]

    boards: Mapped[List["Board"]] = relationship(back_populates="crate")
