from typing import TYPE_CHECKING
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from shep_api_server.database.models.base import Base, timestamp

if TYPE_CHECKING:
    from shep_api_server.database.models.crates import Crate


class Board(Base):
    __tablename__ = "board"

    id: Mapped[str] = mapped_column(primary_key=True, unique=True)
    crate_id: Mapped[str] = mapped_column(ForeignKey("crates.id", name="fk_crate_id"))
    crate: Mapped["Crate"] = relationship(back_populates="boards")
    slot: Mapped[int]
    ip: Mapped[str]
    port: Mapped[int]
    date_registered: Mapped[timestamp]
