import logging
import os
from pathlib import Path
from contextlib import contextmanager
from typing import Iterator

from platformdirs import PlatformDirs
from sqlalchemy import Connection, create_engine
from sqlalchemy.orm import Session, sessionmaker

from shep_api_server.database import models

logger = logging.getLogger(__file__)


class SessionManager:
    def __init__(self, url: str, **kwargs):
        logger.debug(f"using database {url}")
        self._engine = create_engine(
            url, connect_args={"check_same_thread": False}, **kwargs
        )
        self._sessionmaker = sessionmaker(self._engine)

    @contextmanager
    def connect(self) -> Iterator[Connection]:
        with self._engine.begin() as connection:
            try:
                yield connection
            except Exception:
                connection.rollback()
                raise

    @contextmanager
    def session(self) -> Iterator[Session]:
        session = self._sessionmaker()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def create_all(self, connection: Connection):
        models.Base.metadata.create_all(bind=connection)

    def drop_all(self, connection: Connection):
        models.Base.metadata.create_all(bind=connection)


def get_database_path() -> str:
    if "DATABASE_URL" not in os.environ or os.environ["DATABASE_URL"] == "":
        data_dir = Path(PlatformDirs("shep_api_server").user_data_dir)
        data_dir.mkdir(parents=True, exist_ok=True)
        database = data_dir / "database.db"

        database_url = f"sqlite+pysqlite:///{database.absolute()}"
        return database_url

    return os.environ["DATABASE_URL"]


sessionmanager = SessionManager(get_database_path())

__all__ = ["get_database_path", "sessionmanager", "models"]
