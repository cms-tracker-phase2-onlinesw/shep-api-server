from fastapi.routing import APIRouter


def get_router() -> APIRouter:
    from shep_api_server.routers.v1 import get_router as get_router_v1

    routes = APIRouter(prefix="/api")
    routes.include_router(get_router_v1())

    return routes
