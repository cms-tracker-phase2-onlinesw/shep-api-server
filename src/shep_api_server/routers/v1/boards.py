import logging
from typing import List, Sequence

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.orm import Session

from shep_api_server import schemas
from shep_api_server.database import models
from shep_api_server.dependencies import get_database

logger = logging.getLogger(__file__)

router = APIRouter(
    prefix="/boards",
    tags=["boards"],
)


@router.get("/", response_model=List[schemas.Board])
def list(db: Session = Depends(get_database)) -> Sequence[models.Board]:
    results = db.scalars(select(models.Board)).all()
    return results


@router.get("/{id}/", response_model=schemas.Board)
def get(id: str, db: Session = Depends(get_database)) -> models.Board | HTTPException:
    result = db.scalars(select(models.Board).filter(models.Board.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Board not found")
    return result


@router.post("/", response_model=schemas.Board)
def create(
    board: schemas.Board, db: Session = Depends(get_database)
) -> models.Board | HTTPException:
    entry = models.Board(**board.dict())
    db.add(entry)  # Create new entry in database
    db.commit()  # Save entry
    db.refresh(entry)  # Update entry with it's ID
    return entry


@router.delete("/{id}/", response_model=schemas.Board)
def delete(
    id: str, db: Session = Depends(get_database)
) -> models.Board | HTTPException:
    result = db.scalars(select(models.Board).filter(models.Board.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Board not found")
    db.delete(result)
    return result


@router.put("/{id}/", response_model=schemas.Board)
def update_instance(
    id: str,
    input: schemas.BoardUpdate,
    db: Session = Depends(get_database),
) -> schemas.Board:
    current = db.scalars(select(models.Board).filter(models.Board.id == id)).one()
    for key, value in input.dict().items():
        setattr(current, key, value)
    return schemas.Board.from_orm(current)


@router.patch("/{id}/", response_model=schemas.Board)
def patch_instance(
    id: str,
    input: schemas.BoardUpdate,
    db: Session = Depends(get_database),
) -> schemas.Board:
    current = db.scalars(select(models.Board).filter(models.Board.id == id)).one()
    for key, value in input.dict(exclude_unset=True).items():
        setattr(current, key, value)
    return schemas.Board.from_orm(current)
