from fastapi.routing import APIRouter


def get_router() -> APIRouter:
    from shep_api_server.routers.v1.boards import router as Boards
    from shep_api_server.routers.v1.crates import router as Crates
    from shep_api_server.routers.v1.links import router as Links

    routes = APIRouter(prefix="")
    routes.include_router(Boards)
    routes.include_router(Crates)
    routes.include_router(Links)

    return routes
