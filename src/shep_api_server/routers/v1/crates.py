import logging
from typing import List, Sequence

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.orm import Session

from shep_api_server import schemas
from shep_api_server.database import models
from shep_api_server.dependencies import get_database

logger = logging.getLogger(__file__)

router = APIRouter(
    prefix="/crates",
    tags=["crates"],
)


@router.get("/", response_model=List[schemas.Crate])
def list(db: Session = Depends(get_database)) -> Sequence[models.Crate]:
    results = db.scalars(select(models.Crate)).all()
    return results


@router.get("/{id}/", response_model=schemas.Crate)
def get(id: str, db: Session = Depends(get_database)) -> models.Crate | HTTPException:
    result = db.scalars(select(models.Crate).filter(models.Crate.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Crate not found")
    return result


@router.get("/{id}/boards/", response_model=List[schemas.Board])
def get_boards(
    id: str, db: Session = Depends(get_database)
) -> List[models.Board] | HTTPException:
    result = db.scalars(select(models.Crate).filter(models.Crate.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Crate not found")
    return result.boards


@router.post("/", response_model=schemas.Crate)
def create(
    board: schemas.Crate, db: Session = Depends(get_database)
) -> models.Crate | HTTPException:
    entry = models.Crate(**board.dict())
    db.add(entry)  # Create new entry in database
    db.commit()  # Save entry
    db.refresh(entry)  # Update entry with it's ID
    return entry


@router.delete("/{id}/", response_model=schemas.Crate)
def delete(
    id: str, db: Session = Depends(get_database)
) -> models.Crate | HTTPException:
    result = db.scalars(select(models.Crate).filter(models.Crate.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Crate not found")
    db.delete(result)
    return result


@router.put("/{id}/", response_model=schemas.Crate)
def update_instance(
    id: str,
    input: schemas.CrateUpdate,
    db: Session = Depends(get_database),
) -> schemas.Crate:
    current = db.scalars(select(models.Crate).filter(models.Crate.id == id)).one()
    for key, value in input.dict().items():
        setattr(current, key, value)
    return schemas.Crate.from_orm(current)


@router.patch("/{id}/", response_model=schemas.Crate)
def patch_instance(
    id: str,
    input: schemas.CrateUpdate,
    db: Session = Depends(get_database),
) -> schemas.Crate:
    current = db.scalars(select(models.Crate).filter(models.Crate.id == id)).one()
    for key, value in input.dict(exclude_unset=True).items():
        setattr(current, key, value)
    return schemas.Crate.from_orm(current)
