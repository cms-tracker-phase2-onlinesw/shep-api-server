import logging

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.orm import Session

from shep_api_server import schemas
from shep_api_server.database import models
from shep_api_server.dependencies import get_database

logger = logging.getLogger(__file__)

router = APIRouter(
    prefix="/links",
    tags=["links"],
)


@router.get("/", response_model=list[schemas.Link])
def list(db: Session = Depends(get_database)) -> list[schemas.Link]:
    results = db.scalars(select(models.Link)).all()
    return [schemas.Link.from_orm(r) for r in results]


@router.get("/{id}/", response_model=schemas.Link)
def get(id: str, db: Session = Depends(get_database)) -> schemas.Link | HTTPException:
    result = db.scalars(select(models.Link).filter(models.Link.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Crate not found")
    return schemas.Link.from_orm(result)


@router.post("/", response_model=schemas.Link)
def create(
    input: schemas.Link, db: Session = Depends(get_database)
) -> schemas.Link | HTTPException:
    entry = models.Link(**input.dict())
    db.add(entry)  # Create new entry in database
    db.commit()  # Save entry
    db.refresh(entry)  # Update entry with it's ID
    return schemas.Link.from_orm(entry)


@router.delete("/{id}/", response_model=schemas.Link)
def delete(
    id: str, db: Session = Depends(get_database)
) -> schemas.Link | HTTPException:
    result = db.scalars(select(models.Link).filter(models.Link.id == id)).first()
    if result is None:
        raise HTTPException(status_code=404, detail="Crate not found")
    db.delete(result)
    return schemas.Link.from_orm(result)


@router.put("/{id}/", response_model=schemas.Link)
def update_instance(
    id: str,
    input: schemas.LinkUpdate,
    db: Session = Depends(get_database),
) -> schemas.Link:
    current = db.scalars(select(models.Link).filter(models.Link.id == id)).one()
    for key, value in input.dict().items():
        setattr(current, key, value)
    return schemas.Link.from_orm(current)


@router.patch("/{id}/", response_model=schemas.Link)
def patch_instance(
    id: str,
    input: schemas.LinkUpdate,
    db: Session = Depends(get_database),
) -> schemas.Link:
    current = db.scalars(select(models.Link).filter(models.Link.id == id)).one()
    for key, value in input.dict(exclude_unset=True).items():
        setattr(current, key, value)
    return schemas.Link.from_orm(current)
