from fastapi import FastAPI

from shep_api_server.routers import get_router

app = FastAPI()
app.include_router(get_router())


@app.get("/")
async def root():
    return {"message": "Hello World!"}
