import uvicorn


def main():
    server = uvicorn.Server(
        uvicorn.Config("shep_api_server:app", port=8000, log_level="info")
    )
    server.run()


if __name__ == "__main__":
    main()
