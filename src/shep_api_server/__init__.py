import logging

from shep_api_server.application import app

__all__ = ["app"]

logging.basicConfig(level="DEBUG")
logging.getLogger(__name__).addHandler(logging.NullHandler())
