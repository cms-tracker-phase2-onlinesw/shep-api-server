from datetime import datetime
from typing import Optional

from pydantic import Field
from typing_extensions import Annotated

from shep_api_server.schemas.base import Base

NetworkPort = Annotated[int, Field(gt=0, lt=2**16)]


class Board(Base):
    id: str
    crate_id: str
    slot: int
    ip: str
    port: NetworkPort
    date_registered: Optional[datetime]


class BoardUpdate(Base):
    crate_id: Optional[str]
    slot: Optional[int]
    ip: Optional[str]
    port: Optional[NetworkPort]
