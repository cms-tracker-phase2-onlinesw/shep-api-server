from pydantic import BaseModel, Extra


class Base(BaseModel):
    class Config:
        orm_mode = True
        extra = Extra.forbid
