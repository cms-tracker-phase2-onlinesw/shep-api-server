from typing import Optional

from shep_api_server.schemas.base import Base


class Link(Base):
    id: str
    src_board_id: str
    src_processor: str
    src_port: int
    dst_board_id: str
    dst_processor: str
    dst_port: int


class LinkUpdate(Base):
    src_board_id: Optional[str] = None
    src_processor: Optional[str] = None
    src_port: Optional[int] = None
    dst_board_id: Optional[str] = None
    dst_processor: Optional[str] = None
    dst_port: Optional[int] = None
