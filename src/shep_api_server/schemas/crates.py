from typing import Optional

from shep_api_server.schemas.base import Base


class Crate(Base):
    id: str
    building: str
    rack: str
    height: int
    description: str
    csp_id: int


class CrateUpdate(Base):
    building: Optional[str]
    rack: Optional[str]
    height: Optional[int]
    description: Optional[str]
    csp_id: Optional[int]
