from typing import Generator

from sqlalchemy.orm import Session

from shep_api_server.database import sessionmanager


def get_database() -> Generator[Session, None, None]:
    with sessionmanager.session() as session:
        yield session
