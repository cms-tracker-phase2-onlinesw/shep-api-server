#!/usr/bin/env bash
set -eux

ROOT="$PWD/.nix"
export PGDATA="$ROOT/pgdata"
export PGHOST="$ROOT/sockets"
export PGHOSTADDR="0.0.0.0"
export PGPORT="5432"
export PGUSER="postgres"
export PGDATABASE="shepapi"
echo $ROOT

client_pid=$PPID

start_postgres() {
    if postgres_is_stopped
    then
        logfile="$ROOT/log/pg.log"
        mkdir -p "$PGHOST" "${logfile%/*}"
        (set -m
        pg_ctl start --silent -w --log "$logfile" -o "-k $PGHOST -i -h '$PGHOSTADDR'")
    fi
}

postgres_is_stopped() {
    pg_ctl status >/dev/null
    (( $? == 3 ))
}

case "$1" in
    add)
        mkdir -p $ROOT/pids && touch $ROOT/pids/$client_pid
        if [ -d "$PGDATA" ]
        then
            start_postgres
        else
            pg_ctl initdb --silent -o "--auth-host=trust --username=$PGUSER --pwfile='$ROOT/password.txt'" \
            && echo "host all all all password" >> $PGDATA/pg_hba.conf \
            && start_postgres \
            && createdb --username=postgres $PGDATABASE
        fi
        ;; 
    remove)
        rm $ROOT/pids/$client_pid
        if [ -n "$(find $ROOT/pids -prune -empty)" ]
        then
            pg_ctl stop --silent -W
        fi
        ;;
    clean)
        rm -rf $ROOT/sockets $ROOT/pids $ROOT/pgdata $ROOT/log
        ;;
    *)
        echo "Usage: ${BASH_SOURCE[0]##*/} {add | remove}"
        exit 1
        ;;
esac