ARG PYTHON_MAJOR=3
ARG PYTHON_MINOR=10

# Builder
FROM python:$PYTHON_MAJOR.$PYTHON_MINOR-slim AS Builder

# Setup dependant tools
RUN apt update && \
    apt install -y git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip install -U pip setuptools wheel && pip install pdm

# Copy project into container
COPY . /project/
WORKDIR /project
RUN mkdir __pypackages__ && pdm install --prod --no-lock --no-editable

# Runner
FROM python:$PYTHON_MAJOR.$PYTHON_MINOR-slim
ARG PYTHON_MAJOR
ARG PYTHON_MINOR

EXPOSE 8000
ENV PYTHONPATH=/project/pkgs

COPY --from=builder /project/__pypackages__/$PYTHON_MAJOR.$PYTHON_MINOR/lib /project/pkgs
RUN python -m shep_api_server.database.migrations up --rev head --yes
VOLUME /root/.local/share/shep_api_server

CMD ["python", "-m", "uvicorn", "shep_api_server:app", "--host", "0.0.0.0"]
