from fastapi.testclient import TestClient
from shep_api_server.database import SessionManager
from shep_api_server.database.models.links import Link
from sqlalchemy import func


def test_create(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/links/",
        json={
            "id": "linkB",
            "src_board_id": "localhost",
            "src_processor": "x1",
            "src_port": 2,
            "dst_board_id": "localhost",
            "dst_processor": "x0",
            "dst_port": 1,
        },
    )
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "linkB",
        "src_board_id": "localhost",
        "src_processor": "x1",
        "src_port": 2,
        "dst_board_id": "localhost",
        "dst_processor": "x0",
        "dst_port": 1,
    }

    with manager.session() as session:
        assert session.query(func.count(Link.id)).scalar() == 1


def test_create_with_extra_fields(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/links/",
        json={
            "id": "linkB",
            "src_board_id": "localhost",
            "src_processor": "x1",
            "src_port": 2,
            "dst_board_id": "localhost",
            "dst_processor": "x0",
            "dst_port": 1,
            "type": "herd-dummy",
        },
    )
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }


def test_get(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Link(
                id="linkA",
                src_board_id="localhost",
                src_processor="x0",
                src_port=1,
                dst_board_id="localhost",
                dst_processor="x1",
                dst_port=99,
            )
        )

    response = client.get("/api/links/")
    assert response.status_code == 200, response.text
    json = response.json()
    assert len(json) == 1
    assert json == [
        {
            "id": "linkA",
            "src_board_id": "localhost",
            "src_processor": "x0",
            "src_port": 1,
            "dst_board_id": "localhost",
            "dst_processor": "x1",
            "dst_port": 99,
        }
    ]


def test_delete(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Link(
                id="linkA",
                src_board_id="localhost",
                src_processor="x0",
                src_port=1,
                dst_board_id="localhost",
                dst_processor="x1",
                dst_port=99,
            )
        )

    response = client.delete("/api/links/linkA")
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "linkA",
        "src_board_id": "localhost",
        "src_processor": "x0",
        "src_port": 1,
        "dst_board_id": "localhost",
        "dst_processor": "x1",
        "dst_port": 99,
    }

    with manager.session() as session:
        assert session.query(func.count(Link.id)).scalar() == 0



def test_patch(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Link(
                id="linkA",
                src_board_id="localhost",
                src_processor="x0",
                src_port=1,
                dst_board_id="localhost",
                dst_processor="x1",
                dst_port=99,
            )
        )

    response = client.patch("/api/links/linkA", json={"src_port": 42})
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "linkA",
        "src_board_id": "localhost",
        "src_processor": "x0",
        "src_port": 42,
        "dst_board_id": "localhost",
        "dst_processor": "x1",
        "dst_port": 99,
    }


def test_update_invalid_field(manager: SessionManager, client: TestClient):
    response = client.put("/api/links/linkA", json={"type": "herd-dummy"})
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }
