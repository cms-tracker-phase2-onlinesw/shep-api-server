from datetime import datetime

from fastapi.testclient import TestClient
from freezegun import freeze_time

from shep_api_server.database import SessionManager
from shep_api_server.database.models import Board, Crate
from sqlalchemy import func


def test_create(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/crates/",
        json={
            "id": "crate2",
            "building": "B40",
            "rack": "E0B01",
            "height": 10,
            "description": "The 2nd crate",
            "csp_id": 42,
        },
    )
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "crate2",
        "building": "B40",
        "rack": "E0B01",
        "height": 10,
        "description": "The 2nd crate",
        "csp_id": 42,
    }

    with manager.session() as session:
        assert session.query(func.count(Crate.id)).scalar() == 1


def test_create_with_extra_fields(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/crates/",
        json={
            "id": "crate2",
            "building": "B40",
            "rack": "E0B01",
            "height": 10,
            "description": "The 2nd crate",
            "csp_id": 42,
            "type": "herd-dummy",
        },
    )
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }


def test_get(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Crate(
                id="crateX",
                building="B904",
                rack="E1A05",
                height=42,
                description="My new crate",
                csp_id=4,
            )
        )

    response = client.get("/api/crates/")
    assert response.status_code == 200, response.text
    json = response.json()
    assert len(json) == 1
    assert json == [
        {
            "id": "crateX",
            "building": "B904",
            "rack": "E1A05",
            "height": 42,
            "description": "My new crate",
            "csp_id": 4,
        }
    ]


def test_delete(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Crate(
                id="crateX",
                building="B904",
                rack="E1A05",
                height=42,
                description="My new crate",
                csp_id=4,
            )
        )

    response = client.delete("/api/crates/crateX")
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "crateX",
        "building": "B904",
        "rack": "E1A05",
        "height": 42,
        "description": "My new crate",
        "csp_id": 4,
    }

    with manager.session() as session:
        assert session.query(func.count(Crate.id)).scalar() == 0


def test_patch(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Crate(
                id="crateX",
                building="B904",
                rack="E1A05",
                height=42,
                description="My new crate",
                csp_id=4,
            )
        )

    response = client.patch("/api/crates/crateX", json={"rack": "E0B01"})
    assert response.status_code == 200, response.text
    assert response.json() == {
        "id": "crateX",
        "building": "B904",
        "rack": "E0B01",
        "height": 42,
        "description": "My new crate",
        "csp_id": 4,
    }


def test_patch_invalid_field(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Crate(
                id="crateX",
                building="B904",
                rack="E1A05",
                height=42,
                description="My new crate",
                csp_id=4,
            )
        )

    response = client.patch("/api/crates/crateX", json={"type": "herd-dummy"})
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }


@freeze_time("2022-11-17T09:13:08.230807")
def test_get_associated_boards(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        crate = Crate(
            id="crateX",
            building="B904",
            rack="E1A05",
            height=42,
            description="My new crate",
            csp_id=4,
        )
        session.add(crate)

        session.add(
            Board(
                id="localhost",
                slot=5,
                ip="127.0.0.1",
                port=3000,
                date_registered=datetime(2022, 11, 17, 9, 13, 8, 230807),
                crate=crate,
            )
        )

    response = client.get("/api/crates/crateX/boards/")
    assert response.status_code == 200, response.text
    json = response.json()
    assert len(json) == 1
    assert json == [
        {
            "date_registered": "2022-11-17T09:13:08.230807",
            "id": "localhost",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "127.0.0.1",
            "port": 3000,
        }
    ]
