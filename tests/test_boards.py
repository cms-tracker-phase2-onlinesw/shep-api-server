from datetime import datetime

from fastapi.testclient import TestClient
from freezegun import freeze_time
from shep_api_server.database import SessionManager
from sqlalchemy import func

from shep_api_server.database.models import Board
from shep_api_server.database.models.crates import Crate


@freeze_time("2022-11-17T09:13:08.230807")
def test_create(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/boards/",
        json={
            "id": "POD_NAME",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "POD_NAME.herd-dummy.POD_NAMESPACE.svc.cluster.local",
            "port": 3000,
        },
    )
    assert response.status_code == 200, response.text
    assert response.json() == {
        "date_registered": "2022-11-17T09:13:08.230807",
        "id": "POD_NAME",
        "crate_id": "crateX",
        "slot": 5,
        "ip": "POD_NAME.herd-dummy.POD_NAMESPACE.svc.cluster.local",
        "port": 3000,
    }

    with manager.session() as session:
        assert session.query(func.count(Board.id)).scalar() == 1


@freeze_time("2022-11-17T09:13:08.230807")
def test_create_with_ip(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/boards/",
        json={
            "id": "POD_NAME",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "127.0.0.1",
            "port": 3000,
        },
    )
    assert response.status_code == 200, response.text
    assert response.json() == {
        "date_registered": "2022-11-17T09:13:08.230807",
        "id": "POD_NAME",
        "crate_id": "crateX",
        "slot": 5,
        "ip": "127.0.0.1",
        "port": 3000,
    }

    with manager.session() as session:
        assert session.query(func.count(Board.id)).scalar() == 1


@freeze_time("2022-11-17T09:13:08.230807")
def test_create_with_extra_fields(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/boards/",
        json={
            "id": "POD_NAME",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "POD_NAME.herd-dummy.POD_NAMESPACE.svc.cluster.local",
            "port": 3000,
            "type": "herd-dummy",
        },
    )
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }


@freeze_time("2022-11-17T09:13:08.230807")
def test_create_with_invalid_port(manager: SessionManager, client: TestClient):
    response = client.post(
        "/api/boards/",
        json={
            "id": "POD_NAME",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "POD_NAME.herd-dummy.POD_NAMESPACE.svc.cluster.local",
            "port": -3000,
        },
    )
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "ctx": {"limit_value": 0},
                "loc": ["body", "port"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
            }
        ],
    }


@freeze_time("2022-11-17T09:13:08.230807")
def test_create_without_crate(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Crate(
                id="crate1",
                building="B40",
                rack="E0B01",
                height=10,
                description="My crate",
                csp_id=0,
            )
        )

    response = client.post(
        "/api/boards/",
        json={
            "id": "POD_NAME",
            "slot": 5,
            "ip": "POD_NAME.herd-dummy.POD_NAMESPACE.svc.cluster.local",
            "port": 3000,
        },
    )
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "crate_id"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ],
    }


def test_get(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Board(
                id="localhost",
                crate_id="crateX",
                slot=5,
                ip="127.0.0.1",
                port=3000,
                date_registered=datetime(2022, 11, 17, 9, 13, 8, 230807),
            )
        )

    response = client.get("/api/boards/")
    assert response.status_code == 200, response.text
    json = response.json()
    assert len(json) == 1
    assert json == [
        {
            "date_registered": "2022-11-17T09:13:08.230807",
            "id": "localhost",
            "crate_id": "crateX",
            "slot": 5,
            "ip": "127.0.0.1",
            "port": 3000,
        }
    ]


def test_patch(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Board(
                id="localhost",
                crate_id="crateX",
                slot=5,
                ip="127.0.0.1",
                port=3000,
                date_registered=datetime(2022, 11, 17, 9, 13, 8, 230807),
            )
        )

    response = client.patch("/api/boards/localhost/", json={"ip": "10.42.1.1"})
    assert response.status_code == 200, response.text
    assert response.json() == {
        "date_registered": "2022-11-17T09:13:08.230807",
        "id": "localhost",
        "crate_id": "crateX",
        "slot": 5,
        "ip": "10.42.1.1",
        "port": 3000,
    }


def test_patch_invalid_field(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Board(
                id="localhost",
                crate_id="crateX",
                slot=5,
                ip="127.0.0.1",
                port=3000,
                date_registered=datetime(2022, 11, 17, 9, 13, 8, 230807),
            )
        )

    response = client.patch("/api/boards/localhost/", json={"type": "herd-dummy"})
    assert response.status_code == 422, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "type"],
                "msg": "extra fields not permitted",
                "type": "value_error.extra",
            }
        ],
    }


def test_delete(manager: SessionManager, client: TestClient):
    with manager.session() as session:
        session.add(
            Board(
                id="localhost",
                ip="127.0.0.1",
                crate_id="crateX",
                slot=5,
                port=3000,
                date_registered=datetime(2022, 11, 17, 9, 13, 8, 230807),
            )
        )

    response = client.delete("/api/boards/localhost/")
    assert response.status_code == 200, response.text
    assert response.json() == {
        "date_registered": "2022-11-17T09:13:08.230807",
        "id": "localhost",
        "crate_id": "crateX",
        "slot": 5,
        "ip": "127.0.0.1",
        "port": 3000,
    }

    with manager.session() as session:
        assert session.query(func.count(Board.id)).scalar() == 0
