import pytest
from fastapi.testclient import TestClient
from sqlalchemy import StaticPool

from shep_api_server import app
from shep_api_server.database import SessionManager
from shep_api_server.dependencies import get_database


@pytest.fixture(name="manager", scope="function")
def fixture_session_manager() -> SessionManager:
    sm = SessionManager("sqlite+pysqlite:///:memory:", poolclass=StaticPool)
    with sm.connect() as connection:
        print(connection.get_isolation_level())
        sm.create_all(connection)
    return sm


@pytest.fixture(name="client")
def client_fixture():
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="function", autouse=True)
def session(manager: SessionManager):
    def get_database_override():
        with manager.session() as session:
            yield session

    app.dependency_overrides[get_database] = get_database_override
